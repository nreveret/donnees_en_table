# --------- PYODIDE:code --------- #
dico_couleurs = [
    {"nom": "Noir", "rouge": 0, "vert": 0, "bleu": 0},
    {"nom": "Blanc", "rouge": 255, "vert": 255, "bleu": 255},
    {"nom": "Rouge", "rouge": 255, "vert": 0, "bleu": 0},
    {"nom": "Vert", "rouge": 0, "vert": 255, "bleu": 0},
    {"nom": "Bleu", "rouge": 0, "vert": 0, "bleu": 255},
    {"nom": "Jaune", "rouge": 255, "vert": 255, "bleu": 0},
    {"nom": "Cyan", "rouge": 0, "vert": 255, "bleu": 255},
    {"nom": "Magenta", "rouge": 255, "vert": 0, "bleu": 255},
    {"nom": "Argent", "rouge": 192, "vert": 192, "bleu": 192},
    {"nom": "Gris", "rouge": 128, "vert": 128, "bleu": 128},
    {"nom": "Bordeaux", "rouge": 128, "vert": 0, "bleu": 0},
    {"nom": "Olive", "rouge": 128, "vert": 128, "bleu": 0},
    {"nom": "Violet", "rouge": 128, "vert": 0, "bleu": 128},
    {"nom": "Marine", "rouge": 0, "vert": 0, "bleu": 128},
]

# liste_1 est la liste des noms des couleurs
liste_1 = ...

# liste_2 est la liste des composantes vertes des couleurs
liste_2 = ...

# liste_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
liste_3 = ...

# liste_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
liste_4 = ...

# liste_5 est la liste des noms des couleurs dont la composante bleue est supérieure ou égale à la verte et à la rouge
liste_5 = ...

# liste_6 est la liste des triplets formés par les trois composantes de chaque couleur
liste_6 = ...
# --------- PYODIDE:corr --------- #
# liste_1 est la liste des noms des couleurs
liste_1 = [c["nom"] for c in dico_couleurs]

# liste_2 est la liste des composantes vertes des couleurs
liste_2 = [c["vert"] for c in dico_couleurs]

# liste_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
liste_3 = [c["nom"] for c in dico_couleurs if c["rouge"] >= 200]

# liste_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
liste_4 = [c["nom"] for c in dico_couleurs if c["rouge"] > c["bleu"]]

# liste_5 est la liste des noms des couleurs dont la composante bleue est supérieure ou égale à la verte et à la rouge
liste_5 = [c["nom"] for c in dico_couleurs if c["bleu"] >= c["vert"] and c["bleu"] >= c["rouge"]]

# liste_6 est la liste des triplets formés par les trois composantes de chaque couleur
liste_6 = [(c["rouge"], c["vert"], c["bleu"]) for c in dico_couleurs]
# --------- PYODIDE:secrets --------- #
# liste_1 est la liste des noms des couleurs
attendu = [c["nom"] for c in dico_couleurs]
assert liste_1 == attendu, "Erreur sur la liste 1"

# liste_2 est la liste des composantes vertes des couleurs
attendu = [c["vert"] for c in dico_couleurs]
assert liste_2 == attendu, "Erreur sur la liste 2"

# liste_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
attendu = [c["nom"] for c in dico_couleurs if c["rouge"] >= 200]
assert liste_3 == attendu, "Erreur sur la liste 3"

# liste_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
attendu = [c["nom"] for c in dico_couleurs if c["rouge"] > c["bleu"]]
assert liste_4 == attendu, "Erreur sur la liste 4"

# liste_5 est la liste des noms des couleurs dont la composante bleue est plus grande que la verte et la rouge
attendu = [c["nom"] for c in dico_couleurs if c["bleu"] >= c["vert"] and c["bleu"] >= c["rouge"]]
assert liste_5 == attendu, "Erreur sur la liste 5"

# liste_6 est la liste des triplets formés par les trois composantes de chaque couleur
attendu = [(c["rouge"], c["vert"], c["bleu"]) for c in dico_couleurs]
assert liste_6 == attendu, "Erreur sur la liste 6"