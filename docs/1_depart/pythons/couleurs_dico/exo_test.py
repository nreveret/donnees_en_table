# liste_1 est la liste des noms des couleurs
attendu = [c["nom"] for c in dico_couleurs]
assert liste_1 == attendu, "Erreur sur la liste 1"

# liste_2 est la liste des composantes vertes des couleurs
attendu = [c["bleu"] for c in dico_couleurs]
assert liste_2 == attendu, "Erreur sur la liste 2"

# liste_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
attendu = [c["nom"] for c in dico_couleurs if c["rouge"] >= 200]
assert liste_3 == attendu, "Erreur sur la liste 3"

# liste_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
attendu = [c["nom"] for c in dico_couleurs if c["rouge"] >= c["bleu"]]
assert liste_4 == attendu, "Erreur sur la liste 4"

# liste_5 est la liste des noms des couleurs dont la composante bleue est plus grande que la verte et la rouge
attendu = [c["nom"] for c in dico_couleurs if c["bleu"] >= c["vert"] and c["bleu"] >= c["rouge"]]
assert liste_5 == attendu, "Erreur sur la liste 5"

# liste_6 est la liste des triplets formés par les trois composantes de chaque couleur
attendu = [(c["rouge"], c["vert"], c["bleu"]) for c in dico_couleurs]
assert liste_6 == attendu, "Erreur sur la liste 6"
