# --------- PYODIDE:code --------- #
liste_couleurs = [
    ["Noir",       0,   0,   0],
    ["Blanc",    255, 255, 255],
    ["Rouge",    255,   0,   0],
    ["Vert",       0, 255,   0],
    ["Bleu",       0,   0, 255],
    ["Jaune",    255, 255,   0],
    ["Cyan",       0, 255, 255],
    ["Magenta",  255,   0, 255],
    ["Argent",   192, 192, 192],
    ["Gris",     128, 128, 128],
    ["Bordeaux", 128,   0,   0],
    ["Olive",    128, 128,   0],
    ["Violet",   128,   0, 128],
    ["Marine",     0,   0, 128]
]

# liste_1 est la liste des noms des couleurs
liste_1 = ...

# liste_2 est la liste des composantes vertes des couleurs
liste_2 = ...

# liste_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
liste_3 = ...

# liste_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
liste_4 = ...

# liste_5 est la liste des noms des couleurs dont la composante bleue est supérieure ou égale à la verte et à la rouge
liste_5 = ...

# liste_6 est la liste des triplets formés par les trois composantes de chaque couleur
liste_6 = ...
# --------- PYODIDE:corr --------- #
# liste_1 est la liste des noms des couleurs
liste_1 = [c[0] for c in liste_couleurs]

# liste_2 est la liste des composantes vertes des couleurs
liste_2 = [c[2] for c in liste_couleurs]

# liste_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
liste_3 = [c[0] for c in liste_couleurs if c[1] >= 200]

# liste_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
liste_4 = [c[0] for c in liste_couleurs if c[1] > c[3]]

# liste_5 est la liste des noms des couleurs dont la composante bleue est supérieure ou égale à la verte et à la rouge
liste_5 = [c[0] for c in liste_couleurs if c[3] >= c[2] and c[3] >= c[1]]

# liste_6 est la liste des triplets formés par les trois composantes de chaque couleur
liste_6 = [(c[1], c[2], c[3]) for c in liste_couleurs]
# --------- PYODIDE:secrets --------- #
# liste_1 est la liste des noms des couleurs
attendu = [c[0] for c in liste_couleurs]
assert liste_1 == attendu, "Erreur sur la liste 1"

# liste_2 est la liste des composantes vertes des couleurs
attendu = [c[2] for c in liste_couleurs]
assert liste_2 == attendu, "Erreur sur la liste 2"

# liste_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
attendu = [c[0] for c in liste_couleurs if c[1] >= 200]
assert liste_3 == attendu, "Erreur sur la liste 3"

# liste_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
attendu = [c[0] for c in liste_couleurs if c[1] > c[3]]
assert liste_4 == attendu, "Erreur sur la liste 4"

# liste_5 est la liste des noms des couleurs dont la composante bleue est plus grande que la verte et la rouge
attendu = [c[0] for c in liste_couleurs if c[3] >= c[2] and c[3] >= c[1]]
assert liste_5 == attendu, "Erreur sur la liste 5"

# liste_6 est la liste des triplets formés par les trois composantes de chaque couleur
attendu = [(c[1], c[2], c[3]) for c in liste_couleurs]
assert liste_6 == attendu, "Erreur sur la liste 6"

