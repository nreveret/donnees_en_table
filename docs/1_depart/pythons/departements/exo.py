# --------- PYODIDE:env --------- #
departements = {
    "Ain": 5762,
    "Aisne": 7362,
    "Allier": 7340,
    "Alpes-de-Haute-Provence": 6925,
    "Hautes-Alpes": 5549,
    "Alpes-Maritimes": 4299,
    "Ardèche": 5529,
    "Ardennes": 5229,
    "Ariège": 4890,
    "Aube": 6004,
    "Aude": 6139,
    "Aveyron": 8735,
    "Bouches-du-Rhône": 5088,
    "Calvados": 5548,
    "Cantal": 5726,
    "Charente": 5956,
    "Charente-Maritime": 6864,
    "Cher": 7235,
    "Corrèze": 5857,
    "Corse	8": 680,
    "Côte-d'Or": 8763,
    "Côtes-d'Armor": 6878,
    "Creuse": 5565,
    "Dordogne": 9060,
    "Doubs": 5233,
    "Drôme": 6530,
    "Eure": 6040,
    "Eure-et-Loir": 5880,
    "Finistère": 6733,
    "Gard": 5853,
    "Haute-Garonne": 6309,
    "Gers": 6291,
    "Gironde": 9976,
    "Hérault": 6101,
    "Ille-et-Vilaine": 6775,
    "Indre": 6791,
    "Indre-et-Loire": 6127,
    "Isère": 7432,
    "Jura": 4999,
    "Landes": 9243,
    "Loir-et-Cher": 6343,
    "Loire": 4781,
    "Haute-Loire": 4977,
    "Loire-Atlantique": 6809,
    "Loiret": 6775,
    "Lot": 5217,
    "Lot-et-Garonne": 5361,
    "Lozère": 5167,
    "Maine-et-Loire": 7172,
    "Manche": 5938,
    "Marne": 8169,
    "Haute-Marne": 6211,
    "Mayenne": 5175,
    "Meurthe-et-Moselle": 5246,
    "Meuse": 6211,
    "Morbihan": 6823,
    "Moselle": 6216,
    "Nièvre": 6817,
    "Nord": 5743,
    "Oise": 5860,
    "Orne": 6103,
    "Pas-de-Calais": 6671,
    "Puy-de-Dôme": 7970,
    "Pyrénées-Atlantiques": 7645,
    "Hautes-Pyrénées": 4464,
    "Pyrénées-Orientales": 4116,
    "Bas-Rhin": 4755,
    "Haut-Rhin": 3525,
    "Rhône": 3249,
    "Haute-Saône": 5360,
    "Saône-et-Loire": 8575,
    "Sarthe": 6206,
    "Savoie": 6028,
    "Haute-Savoie": 4388,
    "Paris": 105,
    "Seine-Maritime": 6278,
    "Seine-et-Marne": 5915,
    "Yvelines": 2284,
    "Deux-Sèvres": 5999,
    "Somme": 6170,
    "Tarn": 5758,
    "Tarn-et-Garonne": 3718,
    "Var": 5973,
    "Vaucluse": 3567,
    "Vendée": 6720,
    "Vienne": 6990,
    "Haute-Vienne": 5520,
    "Vosges": 5874,
    "Yonne": 7427,
    "Territoire de Belfort": 609,
    "Essonne": 1804,
    "Hauts-de-Seine": 176,
    "Seine-Saint-Denis": 236,
    "Val-de-Marne": 245,
    "Val-d'Oise": 1246,
    "Guadeloupe": 1628,
    "Martinique": 1128,
    "Guyane": 83534,
    "La Réunion": 2504,
}
# --------- PYODIDE:code --------- #
# liste_1 contient tous les noms des départements
liste_1 = ...

# liste_2 contient toutes les surfaces des départements
liste_2 = ...

# liste_3 contient les noms des départements
# strictement mesurant moins de 1000 km²
liste_3 = ...

# liste_4 contient les surfaces des départements
# dont le nom contient la chaîne "Seine"
liste_4 = ...

# liste_5 contient les noms des départements
# dont le nom débute par un "S"
# et la surface est comprise entre 5 000 et 7 000 km²
liste_5 = ...
# --------- PYODIDE:corr --------- #
# liste_1 contient tous les noms des départements
liste_1 = [nom for nom in departements]

# liste_2 contient toutes les surfaces des départements
liste_2 = [departements[nom] for nom in departements]

# liste_3 contient les noms des départements 
# strictement mesurant moins de 1000 km²
liste_3 = [nom for nom in departements if departements[nom] < 1000]

# liste_4 contient les surfaces des départements
# dont le nom contient la chaîne "Seine"
liste_4 = [departements[nom] for nom in departements if "Seine" in nom]

# liste_5 contient les noms des départements
# dont le nom débute par un "S"
# et la surface est comprise entre 5 000 et 7 000 km²
liste_5 = [nom for nom in departements if nom[0] == "S" and 5000 <= departements[nom] <= 7000]
# --------- PYODIDE:secrets --------- #
# Tests
# liste_1 contient tous les noms des départements
attendu = [nom for nom in departements]
assert liste_1 == attendu, "Erreur sur la liste 1"

# liste_2 contient toutes les surfaces des départements
attendu = [departements[nom] for nom in departements]
assert liste_2 == attendu, "Erreur sur la liste 2"

# liste_3 contient les noms des départements
# strictement mesurant moins de 1000 km²
attendu = [nom for nom in departements if departements[nom] < 1000]
assert liste_3 == attendu, "Erreur sur la liste 3"

# liste_4 contient les surfaces des départements
# dont le nom contient la chaîne "Seine"
attendu = [departements[nom] for nom in departements if "Seine" in nom]
assert liste_4 == attendu, "Erreur sur la liste 4"

# liste_5 contient les noms des départements
# dont le nom débute par un "S"
# et la surface est comprise entre 5 000 et 7 000 km²
attendu = [nom for nom in departements if nom[0] == "S" and 5000 <= departements[nom] <= 7000]
assert liste_5 == attendu, "Erreur sur la liste 5"

