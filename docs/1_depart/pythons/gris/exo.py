# --------- PYODIDE:env --------- #
dico_couleurs = {}
# --------- PYODIDE:code --------- #
liste_couleurs = [
    ["Noir", 0, 0, 0],
    ["Blanc", 255, 255, 255],
    ["Rouge", 255, 0, 0],
    ["Vert", 0, 255, 0],
    ["Bleu", 0, 0, 255],
    ["Jaune", 255, 255, 0],
    ["Cyan", 0, 255, 255],
    ["Magenta", 255, 0, 255],
    ["Argent", 192, 192, 192],
    ["Gris", 128, 128, 128],
    ["Bordeaux", 128, 0, 0],
    ["Olive", 128, 128, 0],
    ["Violet", 128, 0, 128],
    ["Marine", 0, 0, 128],
]

niveaux_gris = [0.299 * couleur[...] + 0.587 * ... + ... * ... for couleur in liste_couleurs]
# --------- PYODIDE:corr --------- #
niveaux_gris = [
    0.299 * couleur[1] + 0.587 * couleur[2] + 0.114 * couleur[3] for couleur in liste_couleurs
]
# --------- PYODIDE:secrets --------- #
# Tests
attendus = [
    0.299 * couleur[1] + 0.587 * couleur[2] + 0.114 * couleur[3] for couleur in liste_couleurs
]

assert all(abs(g - a) < 10**-6 for g, a in zip(niveaux_gris, attendus))
