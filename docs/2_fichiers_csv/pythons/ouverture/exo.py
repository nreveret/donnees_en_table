# --------- PYODIDE:env --------- #
url_fichier = "fables.txt"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)
# --------- PYODIDE:code --------- #
fables = []
with open(file=..., mode=..., encoding=...) as fichier:
    for ligne in ...:
        ....append(...)

print(fables[4:29])  # La cigale et la fourmi
# --------- PYODIDE:corr --------- #
fables = []
with open(file="fables.txt", mode="r", encoding="utf-8") as fichier:
    for ligne in fichier:
        fables.append(ligne)
# --------- PYODIDE:secrets --------- #
contenu_ = []
with open(file="fables.txt", mode="r", encoding="utf-8") as fichier:
    for ligne in fichier:
        contenu_.append(ligne)

assert fables == contenu_, "Erreur de lecture"