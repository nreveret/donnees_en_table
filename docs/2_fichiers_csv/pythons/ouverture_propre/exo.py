# --------- PYODIDE:env --------- #
url_fichier = "fables.txt"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)
# --------- PYODIDE:code --------- #
fables = []

# Compléter ici

print(fables[2:25])  # La cigale et la fourmi
# --------- PYODIDE:corr --------- #
fables = []
with open(file="fables.txt", mode="r", encoding="utf-8") as fichier:
    for ligne in fichier:
        ligne_propre = ligne.strip()
        if ligne_propre != "":
            fables.append(ligne_propre)
# --------- PYODIDE:secrets --------- #
# Test
contenu_ = []
with open(file="fables.txt", mode="r", encoding="utf-8") as fichier:
    for ligne in fichier:
        ligne = ligne.strip()
        if ligne:
            contenu_.append(ligne)

assert fables == contenu_, "Erreur de lecture"
