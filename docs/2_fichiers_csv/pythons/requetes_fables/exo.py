# --------- PYODIDE:env --------- #
url_fichier = "fables.txt"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)
# --------- PYODIDE:code --------- #
fables = []
with open(file="fables.txt", mode="r", encoding="utf-8") as fichier:
    for ligne in fichier:
        ligne_propre = ligne.strip()
        if ligne_propre != "":
            fables.append(ligne_propre)

# liste_1 contient les vers dans lequel apparaît la chaîne "mouton"
liste_1 = ...

# liste_2 contient les vers dans lequel apparaît la chaîne "cigale"
liste_2 = ...

# liste_3 contient les vers dans lequel apparaîssent les chaînes "cigale" et "fourmi"
liste_3 = ...

# liste_4 contient les vers longs de 5 caractères ou plus
liste_4 = ...

# liste_5 contient les vers dans lesquels on utilise les guillemets (")
liste_5 = ...

# liste_6 contient les noms des fables
# (ils comptent 3 caractères ou plus et sont en majuscule)
liste_6 = ...
# --------- PYODIDE:corr --------- #
# liste_1 contient les vers dans lequel apparaît la chaîne "mouton"
liste_1 = [vers for vers in fables if "mouton" in vers.lower()]

# liste_2 contient les vers dans lequel apparaît la chaîne "cigale"
liste_2 = [vers for vers in fables if "cigale" in vers.lower()]

# liste_3 contient les vers dans lequel apparaîssent les chaînes "cigale" et "fourmi"
liste_3 = [vers for vers in fables if "cigale" in vers.lower() and "fourmi" in vers.lower()]

# liste_4 contient les vers longs de 5 caractères ou plus
liste_4 = [vers for vers in fables if len(vers) >= 5]

# liste_5 contient les vers dans lesquels on utilise les guillemets (")
liste_5 = [vers for vers in fables if '"' in vers]

# liste_6 contient les noms des fables
# (ils comptent 3 caractères ou plus et sont en majuscule)
liste_6 = [vers for vers in fables if len(vers) >= 3 and vers.upper() == vers]
# --------- PYODIDE:secrets --------- #
# liste_1 contient les vers dans lequel apparaît la chaîne "mouton"
attendu = [vers for vers in fables if "mouton" in vers.lower()]
assert liste_1 == attendu, "Erreur sur la liste 1"

# liste_2 contient les vers dans lequel apparaît la chaîne "cigale"
attendu = [vers for vers in fables if "cigale" in vers.lower()]
assert liste_2 == attendu, "Erreur sur la liste 2"

# liste_3 contient les vers dans lequel apparaîssent les chaînes "cigale" et "fourmi"
attendu = [vers for vers in fables if "cigale" in vers.lower() and "fourmi" in vers.lower()]
assert liste_3 == attendu, "Erreur sur la liste 3"

# liste_4 contient les vers longs de 5 caractères ou plus
attendu = [vers for vers in fables if len(vers) >= 4]
assert liste_4 == attendu, "Erreur sur la liste 4"

# liste_5 contient les vers dans lesquels on utilise les guillemets (")
attendu = [vers for vers in fables if '"' in vers]
assert liste_5 == attendu, "Erreur sur la liste 5"

# liste_6 contient les noms des fables
# (ils comptent 3 caractères ou plus et sont en majuscule)
attendu = [vers for vers in fables if len(vers) >= 3 and vers.upper() == vers]
assert liste_6 == attendu, "Erreur sur la liste 6"
