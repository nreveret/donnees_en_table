# --------- PYODIDE:env --------- #
url_fichier = "temperatures_2020.csv"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)
# --------- PYODIDE:code --------- #
# Import
temperatures = []
with open(file="temperatures_2020.csv", mode=..., encoding="utf-8") as fichier:
    fichier.readline()
    for ligne in ...:
        ligne_propre = ligne....
        valeurs = ligne_propre.split(...)
        temperatures.append(...)

# Typage
for entree in temperatures:
    entree[1] = int(entree[1])
    entree[3] = float(entree[...])
    entree[...] = ...(...[...])
    ...
# --------- PYODIDE:corr --------- #
# Import
temperatures = []
with open(file="temperatures_2020.csv", mode="r", encoding="utf-8") as fichier:
    fichier.readline()
    for ligne in fichier:
        ligne_propre = ligne.strip()
        valeurs = ligne_propre.split(",")
        temperatures.append(valeurs)

# Typage
for entree in temperatures:
    entree[1] = int(entree[1])
    entree[3] = float(entree[3])
    entree[4] = float(entree[4])
    entree[5] = float(entree[5])
# --------- PYODIDE:secrets --------- #
# Import
attendu = []
with open(file="temperatures_2020.csv", mode="r", encoding="utf-8") as fichier:
    fichier.readline()
    for ligne in fichier:
        ligne_propre = ligne.strip()
        valeurs = ligne_propre.split(",")
        attendu.append(valeurs)

# Typage
for entree in attendu:
    entree[1] = int(entree[1])
    entree[3] = float(entree[3])
    entree[4] = float(entree[4])
    entree[5] = float(entree[5])

assert sorted(temperatures) == sorted(attendu), "Erreur d'import ou de typage"