# --------- PYODIDE:env --------- #
url_fichier = "temperatures_2020.csv"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)
# --------- PYODIDE:code --------- #
# Import
import csv
with open("temperatures_2020.csv", "r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=",")
    temperatures = [entree for entree in lecteur]

# Typage
for entree in temperatures:
    entree["jour"] = int(entree["jour"])
    entree["tmin"] = float(entree[...])
    entree[...] = ...(...[...])
    ...
# --------- PYODIDE:corr --------- #
import csv

with open("temperatures_2020.csv", "r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=",")
    temperatures = [entree for entree in lecteur]


# Typage
for entree in temperatures:
    entree["jour"] = int(entree["jour"])
    entree["tmin"] = float(entree["tmin"])
    entree["tmax"] = float(entree["tmax"])
    entree["tmoy"] = float(entree["tmoy"])
# --------- PYODIDE:secrets --------- #
import csv
attendu = []
with open("temperatures_2020.csv", "r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=",")
    for ligne in lecteur:
        attendu.append(ligne)

# Typage
for entree in attendu:
    entree["jour"] = int(entree["jour"])
    entree["tmin"] = float(entree["tmin"])
    entree["tmax"] = float(entree["tmax"])
    entree["tmoy"] = float(entree["tmoy"])

assert temperatures == attendu, "Erreur d'import ou de typage"