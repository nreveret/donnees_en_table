# --------- PYODIDE:env --------- #
url_fichier = "nat2021.csv"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)
# --------- PYODIDE:code --------- #
import csv

prenoms = []
with open(file=..., mode=..., encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=...)
    for entree in ...:
        entree["annee"] = ...
        entree[...] = int(...)
        prenoms.append(...)

for i in range(5):
    print(prenoms[i])  # vérification
# --------- PYODIDE:corr --------- #
import csv
prenoms = []
with open(file="nat2021.csv", mode="r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=";")
    for entree in lecteur:
        entree["annee"] = int(entree["annee"])
        entree["nombre"] = int(entree["nombre"])
        prenoms.append(entree)
# --------- PYODIDE:secrets --------- #
# Tests
import csv
attendu = []
with open(file="nat2021.csv", mode="r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=";")
    for entree in lecteur:
        entree["annee"] = int(entree["annee"])
        entree["nombre"] = int(entree["nombre"])
        attendu.append(entree)

assert prenoms == attendu, "Erreur d'import et/ou de typage"