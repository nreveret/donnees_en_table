# --------- PYODIDE:env --------- #
url_fichier = "films.csv"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)

import csv

films = []
with open("films.csv", "r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=";")
    for entree in lecteur:
        entree["Ranking"] = int(entree["Ranking"])
        entree["Year"] = int(entree["Year"])
        entree["Minutes"] = int(entree["Minutes"])
        entree["Rating"] = float(entree["Rating"])
        entree["Votes"] = int(entree["Votes"])
        entree["Gross"] = float(entree["Gross"])
        films.append(entree)
# --------- PYODIDE:code --------- #
total_family = 0
total_horror = 0

...
        
print(f"{total_family = }")
print(f"{total_horror = }")
print(f"{(total_family > total_horror) = }")
# --------- PYODIDE:corr --------- #
total_family = 0
total_horror = 0

for entree in films:
    if "Family" in entree["Genres"]:
        total_family += entree["Votes"]
    elif "Horror" in entree["Genres"]:
        total_horror += entree["Votes"]
# --------- PYODIDE:secrets --------- #
# Tests aléatoires
attendu_family = 8384520
attendu_horror = 7138023

assert total_family == attendu_family, "Erreur pour le genre 'Family'"
assert total_horror == attendu_horror, "Erreur pour le genre 'Horror'"
