# --------- PYODIDE:env --------- #
url_fichier = "films.csv"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)

import csv

films = []
with open("films.csv", "r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=";")
    for entree in lecteur:
        entree["Ranking"] = int(entree["Ranking"])
        entree["Year"] = int(entree["Year"])
        entree["Minutes"] = int(entree["Minutes"])
        entree["Rating"] = float(entree["Rating"])
        entree["Votes"] = int(entree["Votes"])
        entree["Gross"] = float(entree["Gross"])
        films.append(entree)
# --------- PYODIDE:code --------- #
def somme(donnees, descripteur):
    """Renvoie la somme des valeurs de ce descripteur"""
    return sum([entree[descripteur] for entree in donnees])


def moyenne(donnees, descripteur):
    """Renvoie la moyenne des valeurs de ce descripteur"""
    pass


assert abs(moyenne(films, "Minutes") - 122.57) < 10**-2, "Erreur sur la moyenne de 'Minutes'"
# --------- PYODIDE:corr --------- #
def moyenne(donnees, descripteur):
    """Renvoie la moyenne des valeurs de ce descripteur"""
    return somme(donnees, descripteur) / len(donnees)
# --------- PYODIDE:secrets --------- #
# Tests publics
assert abs(moyenne(films, "Gross") - 63.97) < 10**-3, "Erreur sur la moyenne de 'Gross'"

# Tests aléatoires
def moyenne_corr(donnees, descripteur):
    return sum([entree[descripteur] for entree in donnees]) / len(donnees)


for descripteur in ["Year", "Rating", "Votes", "Minutes", "Gross"]:
    attendu = moyenne_corr(films, descripteur)
    assert abs(moyenne(films, descripteur) - attendu) < 10**-2, f"Erreur sur la moyenne de '{descripteur}'"

entiers = [
    {1: 0, 2: 1, 3: 5},
    {1: 0, 2: 1, 3: 5},
]
assert moyenne(entiers, 1) == 0, "Erreur dans un test secret"
assert moyenne(entiers, 2) == 1, "Erreur dans un test secret"
assert moyenne(entiers, 3) == 5, "Erreur dans un test secret"
