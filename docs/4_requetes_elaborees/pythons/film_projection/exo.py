# --------- PYODIDE:env --------- #
url_fichier = "films.csv"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)

import csv

films = []
with open("films.csv", "r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=";")
    for entree in lecteur:
        entree["Ranking"] = int(entree["Ranking"])
        entree["Year"] = int(entree["Year"])
        entree["Minutes"] = int(entree["Minutes"])
        entree["Rating"] = float(entree["Rating"])
        entree["Votes"] = int(entree["Votes"])
        entree["Gross"] = float(entree["Gross"])
        films.append(entree)


# --------- PYODIDE:code --------- #
def projection(donnees, descripteur):
    """Renvoie la liste des valeurs de ce descripteur"""
    pass


assert projection(films, "Ranking")[4] == 5, "Erreur sur la projection de 'Ranking'"
assert projection(films, "Name")[4] == "Pulp Fiction", "Erreur sur la projection de 'Name'"
assert projection(films, "Year")[4] == 1994, "Erreur sur la projection de 'Year'"


# --------- PYODIDE:corr --------- #
def projection(donnees, descripteur):
    """Renvoie la liste des valeurs de ce descripteur"""
    return [entree[descripteur] for entree in donnees]


# --------- PYODIDE:secrets --------- #
# Tests publics
assert projection(films, "Ranking")[4] == 5, "Erreur sur la projection de 'Ranking'"
assert projection(films, "Name")[4] == "Pulp Fiction", "Erreur sur la projection de 'Name'"
assert projection(films, "Year")[4] == 1994, "Erreur sur la projection de 'Year'"


# Tests aléatoires
def projection_corr(donnees, descripteur):
    return [entree[descripteur] for entree in donnees]


for descripteur in films[0]:
    attendu = projection_corr(films, descripteur)
    assert projection(films, descripteur) == attendu, f"Erreur sur la projection de '{descripteur}'"

neveux = [
    {1: "riri", 2: "fifi", 3: "loulou"},
    {1: "rara", 2: "fafa", 3: "laulau"},
]
assert projection(neveux, 1) == ["riri", "rara"], "Erreur dans un test secret"
assert projection(neveux, 2) == ["fifi", "fafa"], "Erreur dans un test secret"
