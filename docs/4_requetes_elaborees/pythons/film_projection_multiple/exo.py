# --------- PYODIDE:env --------- #
url_fichier = "films.csv"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)

import csv

films = []
with open("films.csv", "r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=";")
    for entree in lecteur:
        entree["Ranking"] = int(entree["Ranking"])
        entree["Year"] = int(entree["Year"])
        entree["Minutes"] = int(entree["Minutes"])
        entree["Rating"] = float(entree["Rating"])
        entree["Votes"] = int(entree["Votes"])
        entree["Gross"] = float(entree["Gross"])
        films.append(entree)


# --------- PYODIDE:code --------- #
def projection_multiple(donnees, descripteurs):
    """Renvoie les valeurs de ces descripteurs"""
    pass


assert projection_multiple(films, ("Name", "Year", "Rating"))[0] == (
    "Citizen Kane",
    1941,
    8.3,
), "Erreur"


# --------- PYODIDE:corr --------- #
def projection_multiple(donnees, descripteurs):
    """Renvoie les valeurs de ces descripteurs"""
    return [tuple(entree[d] for d in descripteurs) for entree in donnees]


# --------- PYODIDE:secrets --------- #
# Tests
assert projection_multiple(films, ("Name", "Year", "Rating"))[0] == (
    "Citizen Kane",
    1941,
    8.3,
), "Erreur"


# Tests supplémentaires
from random import sample, randrange


def projection_multiple_corr(donnees, descripteurs):
    return [tuple(entree[d] for d in descripteurs) for entree in donnees]


cles = list(films[0].keys())
for _ in range(5):
    descripteurs = sample(cles, randrange(1, len(cles)))
    attendu = projection_multiple_corr(films, descripteurs)
    assert (
        projection_multiple(films, descripteurs) == attendu
    ), f"Erreur sur la projection de '{descripteurs}'"
