# --------- PYODIDE:env --------- #
url_fichier = "films.csv"
encodage = "utf-8"

from js import fetch

reponse = await fetch(f"../{url_fichier}")
contenu = await reponse.text()

with open(file=url_fichier, mode="w", encoding=encodage) as fichier:
    fichier.write(contenu)

import csv

films = []
with open("films.csv", "r", encoding="utf-8") as fichier:
    lecteur = csv.DictReader(fichier, delimiter=";")
    for entree in lecteur:
        entree["Ranking"] = int(entree["Ranking"])
        entree["Year"] = int(entree["Year"])
        entree["Minutes"] = int(entree["Minutes"])
        entree["Rating"] = float(entree["Rating"])
        entree["Votes"] = int(entree["Votes"])
        entree["Gross"] = float(entree["Gross"])
        films.append(entree)


# --------- PYODIDE:code --------- #
def somme(donnees, descripteur):
    """Renvoie la somme des valeurs de ce descripteur"""
    pass


assert abs(somme(films, "Gross") - 33072.07) < 10**-6, "Erreur sur la somme de 'Gross'"


# --------- PYODIDE:corr --------- #
def somme(donnees, descripteur):
    """Renvoie la somme des valeurs de ce descripteur"""
    total = 0
    for entree in donnees:
        total += entree[descripteur]
    return total


# --------- PYODIDE:secrets --------- #
# Tests publics
assert abs(somme(films, "Gross") - 33072.07) < 10**-6, "Erreur sur la somme de 'Gross'"


# Tests aléatoires
def somme_corr(donnees, descripteur):
    return sum([entree[descripteur] for entree in donnees])


for descripteur in ["Year", "Rating", "Votes", "Minutes", "Gross"]:
    attendu = somme_corr(films, descripteur)
    assert (
        abs(somme(films, descripteur) - attendu) < 10**-3
    ), f"Erreur sur la somme de '{descripteur}'"

entiers = [
    {1: 0, 2: 1, 3: 5},
    {1: 0, 2: 1, 3: 5},
]
assert somme(entiers, 1) == 0, "Erreur dans un test secret"
assert somme(entiers, 2) == 2, "Erreur dans un test secret"
assert somme(entiers, 3) == 10, "Erreur dans un test secret"
