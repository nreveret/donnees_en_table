---
author: Nicolas Revéret
title: Crédits
---

# 👍🏽 Crédits

Ce site a été réalisé par N. Revéret (`nreveret<at>ac<tiret>rennes.fr`) avec l'aide des membres du groupe *e-nsi*, en particulier M. Coilhac et F. Chambon. Les fichiers sources sont disponibles sur ce [dépôt](https://forge.apps.education.fr/nreveret/donnees_en_table).

L'ensemble des documents sont sous licence [CC-BY-NC-SA 4.0 (Attribution, Utilisation Non Commerciale, ShareAlike)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Le site est hébergé par la [forge des Communs Numériques](https://docs.forge.apps.education.fr/).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Les logos ont été créés par [Freepik - Flaticon](https://www.flaticon.com/free-icons/matrix)
