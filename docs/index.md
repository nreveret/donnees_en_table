---
author: Nicolas Revéret
title: Accueil
---

# 🏡 Données en tables

Ce site propose un cours d'introduction aux traitements des données en table.

Il est destiné en premier lieu aux élèves de la spécialité *Numérique et Sciences Informatiques* de Première et Terminale.

Les seuls prérequis de ce cours sont les connaissances de bases en Python :

* Les variables : types élémentaires (`#!py int`, `#!py str`, `#!py float`), affectations, calculs ;
* Les fonctions : utilisation et définition ;
* Structures conditionnelles : `#!py if... elif... else` ;
* Structures itératives : les boucles bornées, les « *Pour* », et non bornées, les « *Tant que* » ;
* Les listes (création, parcours, lecture et modification d'éléments) ;
* Les dictionnaires (création, parcours, lecture et modification d'éléments) ;
* Les tuples (création, parcours).

Ce cours est divisé en plusieurs parties. Il n'est pas utile de toutes les lire.

* [🏁 Prendre un bon départ](1_depart/1_listes.md) : des rappels sur les listes et les dictionnaires ;
* [📑 Les fichiers](2_fichiers_csv/1_manipulation.md) : comment ouvrir et lire un fichier avec Python, qu'est-ce qu'un fichier `csv` ?
* [🦝 Requêtes simples](3_requetes_simples/1_prenoms.md) : des premières requêtes sur des données en table ;
* [🐻 Requêtes élaborées](4_requetes_elaborees/1_films.md) : calculs de moyennes, recherche d'extremums... ;
* [:scissors: Fusion de tables](5_fusions/1_fusions.md) : regrouper les données dispersées dans plusieurs tables.

Les crédits sont [ici 👍🏽](credits.md).
