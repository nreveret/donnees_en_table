site_name: "Données en table"
site_description: Cours et exercices en NSI sur le traitement de données en tables
site_author: Nicolas Revéret
site_url: https://nreveret.forge.apps.education.fr/donnees_en_table


copyright: |
   <p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/">
   Cours et exercices en NSI sur les données en table. Partage ou adaptation possible selon
    les conditions de la licence <a
   href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1"
   target="_blank" rel="license noopener noreferrer"
   style="display:inline-block;">CC BY-NC-SA 4.0<img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img
   style="height:22px!important;margin-left:3px;vertical-align:text-bottom;"
   src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>

docs_dir: docs

nav:
    - '🏡 Accueil': index.md
    - '🏁 Prendre un bon départ':
        - 'Listes': 1_depart/1_listes.md
        - 'Listes en compréhension': 1_depart/2_comprehension.md
        - 'Dictionnaires': 1_depart/3_dictionnaires.md
        - 'Listes de listes/dictionnaires': 1_depart/4_imbriquees.md
    - '📑 Les fichiers':
        - 'Lire un fichier': 2_fichiers_csv/1_manipulation.md
        - 'Fichiers csv': 2_fichiers_csv/2_fichiers_csv.md
        - 'Import vers listes': 2_fichiers_csv/3_import_listes.md
        - 'Import vers dictionnaires': 2_fichiers_csv/4_import_dico.md
    - '🦝 Requêtes simples':
        - 'Prénoms': 3_requetes_simples/1_prenoms.md
        - 'Communes': 3_requetes_simples/2_communes.md
    - '🐻 Requêtes élaborées': 4_requetes_elaborees/1_films.md
    - '✂ Fusions': 5_fusions/1_fusions.md
    - '👍🏽 Crédits': credits.md

# exclude_docs: Nécessite mkdocs 1.6+ pour fonctionner correctement. Sinon, vous pouvez
# utiliser le plugin mkdocs-exclude à la place.
# Ne surtout pas mettre de commentaires sur la ligne d'exclusion des fichiers python... !
# (exclure les fichiers python n'affecte pas les "python_libs")
exclude_docs: |
  **/*_REM.md
  **/*.py


# Configuration du thème:
theme:
  name: pyodide-mkdocs-theme
  font: false                     # RGPD ; pas de fonte Google
  language: fr                    # français
  favicon: favicon.png            # favicon perso
  logo: logo.png                  # logo perso
  palette:                        # Palettes de couleurs jour/nuit
      - media: "(prefers-color-scheme: light)"
        scheme: default
        primary: light-blue
        accent: light-blue
        toggle:
            icon: material/weather-sunny
            name: Passer au mode nuit
      - media: "(prefers-color-scheme: dark)"
        scheme: slate
        primary: blue
        accent: blue
        toggle:
            icon: material/weather-night
            name: Passer au mode jour
  features:
    - navigation.top
    - toc.integrate
    - header.autohide
#   - navigation.instant    # Ne surtout pas activer cette option !



# Configuration minimale pour les plugins
plugins:
  - awesome-pages:          # Pas indispensable, mais... (pip install mkdocs-awesome-pages-plugin)
      collapse_single_pages: true

  - search

  - pyodide_macros:         # !!REQUIS!!
      on_error_fail: true   # Fortement conseillé...

# - exclude-search:         # APRÈS le plugin search (pip install mkdocs-exclude-search)
#     exclude:              # Pour les fichiers inclus dans le build, que la recherche ne doit pas indexer
#       - bac_a_sable.md


markdown_extensions:

  # Extensions indispensables pour le thème:
  - md_in_html              # !!REQUIS!!
  - admonition              # !!REQUIS!! Blocs colorés:  !!! info "ma remarque"
  - attr_list               # !!REQUIS!! Un peu de CSS et des attributs HTML, ex: { #id .class style="display:none" }
  - pymdownx.details        # !!REQUIS!! Admonition: ??? -> peuvent se déplier ; ???+ -> peuvent se replier.
  - pymdownx.emoji:         # !!REQUIS!! Émojis:  :boom: (utilisé dans les terminaux)
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
  - pymdownx.highlight      # !!REQUIS!! Coloration syntaxique du code
  - pymdownx.inlinehilite   # !!REQUIS!! Coloration syntaxique pour les "code spans": `#!python  code_python`
  - pymdownx.snippets:      # !!REQUIS!! Inclusion de fichiers externe.
      check_paths: true     # Fortement conseillé !
  - pymdownx.superfences:   # !!REQUIS!!
      custom_fences:        # (custom_fences: uniquement si vous comptez utiliser mermaid)
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.arithmatex:    # !!REQUIS!! Pour LaTex
      generic: true
  - pymdownx.striphtml:
      strip_js_on_attributes: false     # !!REQUIS!!
      strip_attributes: ""              # !!REQUIS!!

  # Extensions conseillées:
  - abbr
  - def_list                # Les listes de définition.
  - footnotes               # Notes[^1] de bas de page.  [^1]: ma note.
  - pymdownx.caret          # Passage ^^souligné^^ ou en ^exposant^.
  - pymdownx.mark           # Passage ==surligné==.
  - pymdownx.tilde          # Passage ~~barré~~ ou en ~indice~.
  - pymdownx.keys           # Touches du clavier:  ++ctrl+d++
  - pymdownx.tasklist:      # Cases à cocher  - [ ]  et - [x]
      custom_checkbox: false
      clickable_checkbox: true
  - pymdownx.tabbed:        # Volets glissants:  === "Mon volet"
      alternate_style: true # Meilleure compatibilité pour mobiles
      slugify: !!python/object/apply:pymdownx.slugs.slugify
        kwds:
          case: lower
  # - toc:
  #     toc_depth: 0        # Si vous voulez supprimer la ToC (table of content) des pages


extra_css:
  - xtra/stylesheets/ajustements.css # ajustements

# Si mkdocs est lancé en mode strict (mkdocs ... --strict), les warnings lèveront une erreur
# en utilisant ces réglages :
validation:
  omitted_files: warn
  unrecognized_links: warn
